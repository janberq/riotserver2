#include <iostream>
#include <thread>
#include <boost/asio.hpp>
#include <boost/program_options.hpp>

#include "server.hpp"
#include "common.hpp"

int main(int argc, char **argv)
{
    using namespace boost::asio;
    namespace po = boost::program_options;

    std::size_t thread_count = 10;
    unsigned short port = 8000;

    po::options_description desc("Options");
    desc.add_options()
        ("help,h", "Print help message")
        ("port,p", po::value<unsigned short>(&port), "Port on which the server runs")
        ("threads,t", po::value<std::size_t>(&thread_count), "Thread count");
    po::variables_map vm;

    try
    {
        po::store(po::command_line_parser(argc, argv).options(desc).run(), vm);
        if (vm.count("help"))
        {
            std::cout << "RIOTServer2, a server for RAS IoT network, RIOT. Version: " << RIOT_SERVER_VERSION << std::endl;
            std::cout << "for bug reports: canberk.sonmez.409@gmail.com" << std::endl;
            std::cout << desc << std::endl;
            return 0;
        }
        po::notify(vm);
    }
    catch (po::error &err)
    {
        std::cerr << "cmdline error: " << err.what() << std::endl;
        return 1;
    }

    if (thread_count < 1)
    {
        std::cerr << "error: at least one thread is required!" << std::endl;
        return 1;
    }

    io_service io_serv;
    io_service::work work(io_serv);
    riot::server::server s(io_serv, port);
    signal_set signals(io_serv, SIGINT, SIGTERM);
    signals.async_wait([&io_serv]
        (const boost::system::error_code& error, int signal_number)
        {
            io_serv.stop();
        });
    std::vector<std::thread> threads(thread_count);
    for (auto &t: threads) t = std::thread([&] { io_serv.run(); });    
    for (auto &t: threads) if (t.joinable()) t.join();
    return 0;
}
