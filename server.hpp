#ifndef _server_hpp_included
#define _server_hpp_included

#include "common.hpp"
#include "device.hpp"

#include <vector>
#include <list>
#include <memory>
#include <mutex>
#include <set>
#include <string>

namespace riot { namespace server {

class server
{
public:

    server(boost::asio::io_service& io_service, short int portnum);

    void send_event(
        const std::string &dname,   /* name of the sending device */
        const std::string &dtype,   /* type of the sending device (to be checked by others) */
        xeid_matcher &xeidm,        /**
                                     * event to be sent,
                                     * xeidm.eid is checked by target device and
                                     * other devices must satisfy xeidm.match_device */
        buffer_ptr_t data);         /* event data */

    void remove_expired_ptrs();

    ba::io_service &get_io_service();

    bool occupy_name(const std::string &s);
    std::string suggest_new_name(const std::string &s);
    void free_name(const std::string &s);

private:

    ba::io_service &io_service_;
    acceptor_t acceptor_;
    connection_ptr connection_;

    std::list<connection_wptr> connections_;
    std::mutex connections_mtx_;

    std::set<std::string> occupied_names_;
    std::mutex occupied_names_mtx_;

private:

    void do_accept();

};

}}

#endif // _server_hpp_included
