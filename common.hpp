#ifndef _common_hpp_included
#define _common_hpp_included

#include <vector>
#include <memory>
#include <boost/asio.hpp>

// i wish that this is fine
constexpr auto RIOT_SERVER_VERSION = "2.0.1a00";

namespace riot { namespace server {

namespace ba = boost::asio;

using buffer_t = std::vector<char>;
using buffer_ptr_t = std::shared_ptr<buffer_t>;

class connection;
class server;

using connection_ptr    = std::shared_ptr<connection>;
using connection_wptr   = std::weak_ptr<connection>;

using socket_t = ba::ip::tcp::socket;
using acceptor_t = ba::ip::tcp::acceptor;
using endpoint_t = ba::ip::tcp::endpoint;
using boost_err = boost::system::error_code;
using ba::ip::tcp;

bool valid_identifier(const std::string &s);

}}

#endif // _common_hpp_included
