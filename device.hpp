#ifndef _device_hpp_included
#define _device_hpp_included

#include "xeid_matcher.hpp"

#include <string>
#include <list>
#include <tuple>

namespace riot { namespace server {

class device
{
public:
    std::string name;
    std::string type;
    struct subscription {
        std::size_t ID {0};
        xeid_matcher xeidm;
    };
    std::list<subscription> subscribed_evts;
    std::list<subscription> negsubscribed_evts;
};

}}

#endif // _device_hpp_included
