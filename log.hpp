#ifndef _log_hpp_included
#define _log_hpp_included

#include <ostream>
#include <sstream>
#include <utility>
#include <vector>

namespace riot { namespace log {

extern bool _log_enabled;
void _do_log(const std::string &s);

template <typename ...A>
void log_raw(A && ...a)
{
    std::stringstream ss;
    using helper = int [];
    (void) helper{ 0, ( (void)(ss << std::forward<A>(a)), 0 ) ... };
    ss << "\n"; // append it here: thread safety!
    _do_log(ss.str());
}

template <typename ...A>
void log(A && ...a)
{ if (_log_enabled) log_raw("[  LOG  ] ", std::forward<A>(a)...); }

// same as log, but cannot be silenced
template <typename ...A>
void xlog(A && ...a)
{ log_raw("[ LOG ] ", std::forward<A>(a)...); }

template <typename ...A>
void debug(A && ...a)
{ log_raw("[ DBG ] ", std::forward<A>(a)...); }

template <typename ...A>
void error(A && ...a)
{ log_raw("[ ERR ] ", std::forward<A>(a)...); }

template <typename ...A>
void warning(A && ...a)
{ log_raw("[ WRN ] ", std::forward<A>(a)...); }

void log_enable(bool enabled = true);
bool log_enabled();

}}

// to output buffer contents in the log
std::ostream &operator<<(std::ostream &os, const std::vector<char> &vec);

#endif // _log_hpp_included
