#ifndef _connection_hpp_included
#define _connection_hpp_included

#include "common.hpp"
#include "device.hpp"
#include "log.hpp"

#include <utility>
#include <memory>
#include <list>
#include <queue>

namespace riot { namespace server {

class connection : public std::enable_shared_from_this<connection>
{
public:

    device dev;

public:

    connection(server &server);
    socket_t &get_socket();

    void start();
    void write(buffer_ptr_t buffer);
    void writeln(const std::string &s);

    ~connection();

private:

    template <typename ...A>
    void do_log(A&& ...a)
    { log::log("dname: ", dev.name, " dtype: ", dev.type, " ", std::forward<A>(a)...); }

    void do_read();
    void do_write();

    enum {
        STATE_DESCRIPTION,
        STATE_ACTIVE
    } state_;

    server &server_;
    ba::io_service &io_service_;

    socket_t socket_;
    ba::streambuf buffer_;

    ba::strand write_strand_;
    std::queue<
        buffer_ptr_t,
        std::list<buffer_ptr_t>> write_queue_;

};

}}

#endif // _connection_hpp_included
