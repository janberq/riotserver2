#include "server.hpp"
#include "connection.hpp"

#include <algorithm>
#include <sstream>
#include <random>
#include <chrono>

namespace riot { namespace server {

server::server(ba::io_service& io_service, short portnum)
    :
    io_service_(io_service),
    acceptor_(io_service_, endpoint_t(tcp::v4(), portnum))
{
    do_accept();
}

ba::io_service & server::get_io_service()
{
    return io_service_;
}

struct create_event_buffer_t {
    std::size_t s10nID;
    std::string eid;
    std::string dname;
    std::string dtype;
    create_event_buffer_t() {}
    create_event_buffer_t(
        std::size_t s10nID_,
        const std::string &eid_,
        const std::string &dname_,
        const std::string &dtype_) {
        s10nID = s10nID_;
        eid = eid_;
        dname = dname_;
        dtype = dtype_;
    }
};

/**
* event s10nID EID@DNAME#DTYPE s10nID EID@DNAME#DTYPE ... csize
*/
static buffer_ptr_t create_event_buffer(
    const std::list<create_event_buffer_t> &s10nids,
    buffer_ptr_t data)
{
    std::size_t data_sz = 0;
    if (data != nullptr)
        data_sz = data->size();
    std::stringstream ss;
    ss << "event ";
    for (auto it = s10nids.begin(); it != s10nids.end(); it++) {
        ss << it->s10nID << " " << it->eid << "@" << it->dname << "#" << it->dtype;
        if (it != --(s10nids.end())) {
            ss << " ";
        }
    }
    ss << " " << data_sz << "\n";
    std::string header = ss.str();
    std::size_t sz = header.size() + data_sz + 1 /* for the trailing new line */;
    auto buf = std::make_shared<buffer_t>(sz);
    std::copy(header.begin(), header.end(), buf->begin()); // insert the header first
    if (data != nullptr)
        std::copy(data->begin(), data->end(), buf->begin() + header.size());
    buf->back() = '\n'; // insert the trailing endline
    return buf;
}

void server::send_event(const std::string &dname, const std::string &dtype, xeid_matcher& xeidm, buffer_ptr_t data)
{
    std::unique_lock<std::mutex> lock(connections_mtx_);
    for (auto it = connections_.begin(); it != connections_.end(); )
    {
        if (auto c = it->lock())
        {
            if (c->dev.name != dname)
            {
                std::list<create_event_buffer_t> s10n_targetIDs;
                for (auto &s10n_target: c->dev.subscribed_evts)
                {
                    // check if
                    //  1-) the sender device satisfies the target devices conditions
                    //  2-) the target device satisfies the sender devices conditions
                    if (
                        s10n_target.xeidm.matches(xeidm.eid, dname, dtype) &&
                        xeidm.device_matches(c->dev.name, c->dev.type)
                    ) {
                        // now check if the sender dname/dtype/event id are not negsubscribed
                        bool negsubscribe = false;
                        for (auto &ns10n_target: c->dev.negsubscribed_evts)
                        {
                            if (ns10n_target.xeidm.matches(xeidm.eid, dname, dtype)) {
                                negsubscribe = true;
                                break;
                            }
                        }
                        if (!negsubscribe)
                            s10n_targetIDs.emplace_back(s10n_target.ID, xeidm.eid, dname, dtype);
                        else
                            continue;   // check for others
                    }
                }
                if (!s10n_targetIDs.empty())
                    c->write(create_event_buffer(
                        s10n_targetIDs,
                        data));
            }
            ++it;
        }
        else
        {
            auto ittemp = it;
            ++it;
            connections_.erase(ittemp);
        }
    }
}

bool server::occupy_name(const std::string& s)
{
    std::lock_guard<std::mutex> lock(occupied_names_mtx_);
    if (occupied_names_.find(s) == occupied_names_.end())
    {
        // it is NOT occupied!
        occupied_names_.insert(s);
        return true;
    }
    return false;
}

static
std::string random_str(std::size_t len = 4)
{
    static auto eng = std::mt19937(std::chrono::system_clock::now().time_since_epoch().count());
    std::uniform_int_distribution<char> dist(48 + 0, 48 + 9);
    std::string s(4, ' ');
    for (int i = 0; i < len; ++i)
    {
        s[i] = dist(eng);
    }
    return s;
}

std::string server::suggest_new_name(const std::string &s)
{
    if (occupy_name(s))
        return s;
    std::string name;
    while (!occupy_name(name = s + "_" + random_str()));
    return name;
}

void server::free_name(const std::string& s)
{
    std::lock_guard<std::mutex> lock(occupied_names_mtx_);
    occupied_names_.erase(s);
}

void server::remove_expired_ptrs()
{
    if (connections_mtx_.try_lock())
    {
        for (auto it = connections_.begin(); it != connections_.end(); )
        {
            // after I delete the iterator, it becomes not incrementable
            if (it->expired())
            {
                auto ittemp = it;
                ++it;
                connections_.erase(ittemp);
            }
            else
                ++it;
        }
        connections_mtx_.unlock();
    }
}

void server::do_accept()
{
    connection_ = std::make_shared<connection>(*this);
    acceptor_.async_accept(connection_->get_socket(),
        [this](const boost_err &e)
        {
            if (e) return ;
            connection_->start();
            {
                std::unique_lock<std::mutex> lock(connections_mtx_);
                connections_.push_back(connection_);
            }
            do_accept();
        });
}
    
}}
