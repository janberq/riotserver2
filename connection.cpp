#include "connection.hpp"
#include "server.hpp"

#include <istream>
#include <string>

namespace riot { namespace server {

connection::connection(server &server)
    :
    state_{STATE_DESCRIPTION},
    server_(server),
    io_service_(server_.get_io_service()),
    socket_(io_service_),
    write_strand_(io_service_)
{
}

socket_t &connection::get_socket()
{
    return socket_;
}

void connection::start()
{
    log::log("new connection!");
    do_read();
}

void connection::write(buffer_ptr_t buffer)
{
    write_strand_.post([this, buffer, c = shared_from_this()]() {
        write_queue_.push(buffer);
        if (write_queue_.size() > 1) return ; // another write is going on
        do_write();
    });
}

void connection::writeln(const std::string &s)
{
    auto ns = s + "\n";
    auto buf = std::make_shared<buffer_t>(ns.begin(), ns.end());
    write(buf);
}

connection::~connection()
{
    do_log("disconnected");
    server_.free_name(dev.name);
    // would it work really? i mean here are weak_ptr's expired?
    // http://stackoverflow.com/questions/41851520/weak-ptrexpired-behavior-in-the-dtor-of-the-object/41851940#41851940
    // it seems to work, but this is NOT guaranteed with standards!
    // TODO find a better way!
    // (or maybe we don't need?, remove expired ones while sending event data, as we use lists?)
    server_.remove_expired_ptrs();
}

void connection::do_read()
{
    ba::async_read_until(
        socket_,
        buffer_,
        '\n',
        [this, c = shared_from_this()](
            const boost_err &err,
            std::size_t /* bytes_transferred */)
        {
            if (err)
                // disconnected?
                return ;
            std::istream is(&buffer_);
            if (state_ == STATE_DESCRIPTION)
            {
                std::string command;
                if (is >> command)
                {
                    if (command == "action")
                    {
                        if (dev.name.empty())
                        {
                            writeln("error EMPTY_NAME");
                            do_log("device tried to activate with empty name!");
                        }
                        else
                        {
                            state_ = STATE_ACTIVE;
                            writeln("ok");
                            do_log("device activated!");
                        }
                    }
                    else if (command == "serverv")
                    {
                        writeln(RIOT_SERVER_VERSION);
                        do_log("device requested server version: ", RIOT_SERVER_VERSION);
                    }
                    else if (command == "name:")
                    {
                        std::string param;
                        if (is >> param)
                        {
                            if (valid_identifier(param))
                            {
                                if (!server_.occupy_name(param))
                                {
                                    writeln("error NAME_ALREADY_USED");
                                    do_log("device tried to acquire an occupied name: ", param);
                                }
                                else
                                {
                                    // always free the name
                                    if (!dev.name.empty())
                                        server_.free_name(dev.name);
                                    dev.name = param;
                                    writeln("ok");
                                    do_log("device changed its name: ", param);
                                }
                            }
                            else
                            {
                                writeln("error NAME_INVALID");
                                do_log("device tried to acquire an invalid name: ", param);
                            }
                        }
                        else
                        {
                            writeln("error EMPTY_PARAM name");
                            do_log("device didn't provide a name to change");
                        }
                    }
                    else if (command == "xname:")
                    {
                        std::string param;
                        if (is >> param)
                        {
                            if (valid_identifier(param))
                            {
                                if (!dev.name.empty())
                                    server_.free_name(dev.name);    // always free the old name
                                dev.name = server_.suggest_new_name(param);
                                writeln("ok name: " + dev.name);
                                do_log("device changed its name to a suggested one: ", dev.name);
                            }
                            else
                            {
                                writeln("error NAME_INVALID");
                                do_log("device tried to acquire an invalid name: ", param);
                            }
                        }
                        else
                        {
                            writeln("error EMPTY_PARAM name");
                            do_log("device didn't provide a name to change");
                        }
                    }
                    else if (command == "type:")
                    {
                        std::string param;
                        if (is >> param)
                        {
                            if (valid_identifier(param))
                            {
                                dev.type = param;
                                writeln("ok");
                                do_log("device changed its type: ", param);
                            }
                            else
                            {
                                writeln("error TYPE_INVALID");
                                do_log("device tried to acquire an invalid type: ", param);
                            }
                        }
                        else
                        {
                            writeln("error EMPTY_PARAM type");
                            do_log("device didn't provide a type to change");
                        }
                    }
                    else
                    {
                        writeln("error INVALID_CMD");
                        do_log("device tried to execute an invalid command: ", command);
                    }
                }
                else
                {
#ifdef EMPTY_CMD_IS_ERROR // I don't think it's a good idea to make it an error, for now.
                    writeln("error EMPTY_CMD");
                    do_log("device sent empty command");
#endif
                }
            }
            else /* STATE_ACTIVE */
            {
                std::string command;
                if (is >> command)
                {
                    if (command == "subscribe")
                    {
                        std::string xeid;
                        if (is >> xeid)
                        {
                            device::subscription s10n;
                            try
                            {
                                s10n.ID = (dev.subscribed_evts.size() == 0) ?
                                            1 : (dev.subscribed_evts.back().ID + 1);
                                            // better ways to assign them?
                                s10n.xeidm.init(xeid);   // might throw here!
                                dev.subscribed_evts.push_back(s10n);
                                writeln((std::string) "ok s10nID: " + std::to_string(s10n.ID));
                                    // TODO I wish to_string doesn't fuck things up
                                do_log("device subscribed to: ", xeid);
                            }
                            catch (std::regex_error &e)
                            {
                                writeln((std::string) "error BAD_REGEX " + e.what());
                                do_log("device subscribe error, bad regex: ", e.what());
                            }
                        }
                        else
                        {
                            writeln("error EMPTY_PARAM xeid");
                            do_log("device didn't provide a xeid to subscribe");
                        }
                    }
                    else if (command == "unsubscribe") {
                        std::size_t s10nID;
                        if (is >> s10nID)
                        {
                            bool removed = false;
                            auto it = dev.subscribed_evts.begin();
                            auto end = dev.subscribed_evts.end();
                            for (; it != end; ++it) {
                                if (it->ID == s10nID) {
                                    dev.subscribed_evts.erase(it);
                                    removed = true;
                                    break;
                                }
                            }
                            if (removed) {
                                writeln("ok");
                                do_log("device unsubscribed from event" + std::to_string(s10nID));
                            }
                            else {
                                writeln("error S10NID_DOESNT_EXIST");
                                do_log("device cannot have unsubscribed from event" + std::to_string(s10nID) + " s10nID doesn't exist");
                            }
                        }
                        else
                        {
                            writeln("error EMPTY_PARAM s10nID");
                            do_log("device didn't provide an s10nID to unsubscribe");
                        }
                    }
                    // copy and paste the two cases above, then replace subscribe with negsubscribe and s10n with ns10n S10N with NS10N
                    else if (command == "negsubscribe")
                    {
                        std::string xeid;
                        if (is >> xeid)
                        {
                            device::subscription ns10n;
                            try
                            {
                                ns10n.ID = (dev.negsubscribed_evts.size() == 0) ?
                                            1 : (dev.negsubscribed_evts.back().ID + 1);
                                            // better ways to assign them?
                                ns10n.xeidm.init(xeid);   // might throw here!
                                dev.negsubscribed_evts.push_back(ns10n);
                                writeln((std::string) "ok ns10nID: " + std::to_string(ns10n.ID));
                                    // TODO I wish to_string doesn't fuck things up
                                do_log("device negsubscribed to: ", xeid);
                            }
                            catch (std::regex_error &e)
                            {
                                writeln((std::string) "error BAD_REGEX " + e.what());
                                do_log("device negsubscribe error, bad regex: ", e.what());
                            }
                        }
                        else
                        {
                            writeln("error EMPTY_PARAM xeid");
                            do_log("device didn't provide a xeid to negsubscribe");
                        }
                    }
                    else if (command == "unnegsubscribe") {
                        std::size_t ns10nID;
                        if (is >> ns10nID)
                        {
                            bool removed = false;
                            auto it = dev.negsubscribed_evts.begin();
                            auto end = dev.negsubscribed_evts.end();
                            for (; it != end; ++it) {
                                if (it->ID == ns10nID) {
                                    dev.negsubscribed_evts.erase(it);
                                    removed = true;
                                    break;
                                }
                            }
                            if (removed) {
                                writeln("ok");
                                do_log("device unnegsubscribed from event" + std::to_string(ns10nID));
                            }
                            else {
                                writeln("error NS10NID_DOESNT_EXIST");
                                do_log("device cannot have unnegsubscribed from event" + std::to_string(ns10nID) + " ns10nID doesn't exist");
                            }
                        }
                        else
                        {
                            writeln("error EMPTY_PARAM ns10nID");
                            do_log("device didn't provide an ns10nID to unnegsubscribe");
                        }
                    }
                    else if (command == "trig")
                    {
                        std::size_t content_sz;
                        std::string trig_evt;
                        if (is >> trig_evt)
                        {
                            try {
                                xeid_matcher xeidm(trig_evt);
                                if (is >> content_sz)
                                {
                                    /*
                                    * trig EVT_TEST 7      (1)
                                    * canberk              (2)
                                    */
                                    /* content size is given */
                                    if (content_sz == 0) // no data
                                    {
                                        writeln("ok");
                                        do_log("device triggered an event: ", trig_evt, " with no data");
                                        server_.send_event(dev.name, dev.type, xeidm, nullptr);
                                    }
                                    else
                                    {
                                        writeln("ok");
                                        // at this point, we are likely to do some interesting stuff
                                        // we use async_read_until for our ordinary stuff, AND, we use the same streambuf
                                        // for that purpose
                                        // normally, async_read_until reads much more than requested, but if you use
                                        // subsequent async_read_until's with same streambuf, it's fine
                                        // anything more than requested will be stored in it, and the application will just
                                        // work as expected.
                                        // but when async_read(...) comes into the picture, things complicate a little
                                        // now, we need to consider the extra data stored in our streambuf as well
                                        std::string line;
                                        std::getline(is, line); // digest line (1)
                                        char *data_ptr;
                                        auto data_buffer = std::make_shared<std::vector<char>>(content_sz);
                                        data_ptr = &(data_buffer->front());
                                        is.read(data_ptr, content_sz);
                                        std::size_t already_read = is.gcount();
                                        if (already_read < content_sz)
                                        {
                                            ba::async_read(
                                                socket_,
                                                ba::buffer(
                                                    data_ptr + already_read,
                                                    content_sz - already_read),
                                                [this, c = shared_from_this(), trig_evt, content_sz, data_buffer]
                                                (const boost_err &err, std::size_t /* bytes_transferred */)
                                                {
                                                    if (err)
                                                        // disconnected
                                                        return;
                                                    writeln("ok");
                                                    do_log("device triggered an event: ", trig_evt, " with data:\n", *data_buffer);
                                                    xeid_matcher xeidm(trig_evt);
                                                    server_.send_event(dev.name, dev.type, xeidm, data_buffer);
                                                    do_read();
                                                });
                                            return ; // no new do_read
                                        }
                                        else
                                        {
                                            writeln("ok");
                                            do_log("device triggered an event: ", trig_evt, " with data:\n", *data_buffer);
                                            xeid_matcher xeidm(trig_evt);
                                            server_.send_event(dev.name, dev.type, xeidm, data_buffer);
                                        }
                                    }
                                }
                                else
                                    // FIXME there are some inconsistencies in this case, so investigate!
                                    // its better not to use the server in this mode for now, 13 Feb 2017
                                {
                                    writeln("ok DIGEST_LINE");
                                    /*
                                    * trig EVT_TEST        (1)
                                    * canberk sönmez       (2)
                                    * 
                                    */
                                    /* no content size is given, read data until new line */
                                    std::string line;
                                    std::getline(is, line); // digest line (1)
                                    ba::async_read_until(
                                        socket_,
                                        buffer_,
                                        '\n',
                                        [this, c = shared_from_this(), trig_evt](
                                            const boost_err &err,
                                            std::size_t /* bytes_transferred */)
                                        {
                                            if (err)
                                                // disconnected
                                                return ;
                                            xeid_matcher xeidm(trig_evt);
                                            std::istream is(&buffer_);
                                            std::string line;
                                            std::getline(is, line);
                                            auto data_buffer = std::make_shared<std::vector<char>>(line.begin(), line.end());
                                            server_.send_event(dev.name, dev.type, xeidm, data_buffer);
                                            writeln("ok");
                                            do_log("device triggered an event: ", trig_evt, " with data:\n", line);
                                            do_read();
                                        });
                                    return ;    // no new do_read
                                }
                            } catch (std::regex_error &e) {
                                writeln((std::string) "error BAD_REGEX " + e.what());
                                do_log("trig error, bad regex: ", e.what());
                            }
                        }
                        else
                        {
                            writeln("error EMPTY_PARAM trig_evt");
                            do_log("device triggered an event with no name");
                        }
                    }
                    else
                    {
                        writeln("error INVALID_CMD");
                        do_log("device tried to execute an invalid command: ", command);
                    }
                }
                else
                {
#ifdef EMPTY_CMD_IS_ERROR
                    writeln("error EMPTY_CMD");
                    do_log("device sent empty command");
#endif
                }
            }
            do_read();
        });
}

void connection::do_write()
{
    ba::async_write(
        socket_,
        ba::buffer(*write_queue_.front()),
        write_strand_.wrap([this, c = shared_from_this()](
            const boost_err& err,
            std::size_t /* transferred */) {
            write_queue_.pop();
            if (err)
                // we are probably disconnected here, do nothing and return
                return ;
            if (!write_queue_.empty())
                do_write();
        }));
}

}}

