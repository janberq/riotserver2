#include "log.hpp"

#include <iostream>

// initially logs are outputted
namespace riot { namespace log { bool _log_enabled = true; }}

void riot::log::log_enable(bool enabled)
{ _log_enabled = enabled; }

bool riot::log::log_enabled()
{ return _log_enabled; }

void riot::log::_do_log(const std::string& s)
{
    std::cout << s;
    std::cout.flush();
}

std::ostream & operator<<(std::ostream& os, const std::vector<char> &vec)
{
    os.write(&(vec.front()), vec.size());
    return os;
}
